from dataclasses import dataclass
from enums import IssueState, IssueLabel, EventAction
from typing import Any


@dataclass
class GitLabFileResponse:
    status: int
    content: Any = None
    message: str | None = None


@dataclass
class BaseDetail:
    project_id: int
    iid: int
    git_path: str


@dataclass
class IssueDetail(BaseDetail):
    username: str | None
    user_id: int
    state: IssueState

    @classmethod
    def from_issue_body(cls, issue_body: dict):
        assignees = issue_body.get('assignees', [])
        username = assignees[0]['username'] if assignees else None
        return cls(
            project_id=issue_body['project']['id'],
            iid=issue_body['object_attributes']['iid'],
            git_path=issue_body['project']['path_with_namespace'],
            username=username,
            user_id=issue_body['user']['id'],
            state=IssueState(issue_body['object_attributes'].get('state')),
        )


@dataclass
class MergeRequestDetail(BaseDetail):
    action: EventAction
    author_id: int
    changes: dict

    @classmethod
    def from_mr_body(cls, mr_body: dict):
        return cls(
            project_id=mr_body['project']['id'],
            iid=mr_body['object_attributes']['iid'],
            git_path=mr_body['project']['path_with_namespace'],
            action=EventAction(mr_body['object_attributes']['action']),
            author_id=mr_body['object_attributes']['author_id'],
            changes=mr_body.get('changes', {}),
        )


@dataclass
class EventBridgeDetail:
    project_id: int
    iid: int


@dataclass
class EventBridgeAddLabelDetail(EventBridgeDetail):
    # BillingMonthラベルが動的なためstrも定義している
    add_label: IssueLabel | str


@dataclass
class EventBridgeSetAssigneesDetail(EventBridgeDetail):
    user_id: int
