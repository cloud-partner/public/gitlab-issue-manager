from enum import Enum


def enum_to_str(obj):
    if isinstance(obj, Enum):
        return obj.value
    raise TypeError(
        f"Object of type {
            obj.__class__.__name__
        } is not JSON serializable"
    )
