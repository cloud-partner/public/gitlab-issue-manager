from enum import Enum
import os


# TODO: _missing_のUNKOWNはリファクタリングの余地ありそう（全然DRYじゃない）
class ObjectKind(Enum):
    ISSUE = 'issue'
    MERGE_REQUEST = 'merge_request'
    WORK_ITEM = 'work_item'
    UNKNOWN = 'unknown'

    @classmethod
    def _missing_(cls, value: object):
        return cls.UNKNOWN


class EventAction(Enum):
    OPEN = 'open'
    UPDATE = 'update'
    CLOSE = 'close'
    REOPEN = 'reopen'
    MERGE = 'merge'
    UNKNOWN = 'unknown'

    @classmethod
    def _missing_(cls, value: object):
        return cls.UNKNOWN


class IssueState(Enum):
    OPEN = 'opened'
    CLOSED = 'closed'
    UNKNOWN = 'unknown'

    @classmethod
    def _missing_(cls, value: object):
        return cls.UNKNOWN


class LabelChangeState(Enum):
    CURRENT = 'current'
    PREVIOUS = 'previous'


class IssueLabelPrefix(Enum):
    STATUS = 'Status'
    HEALTH = 'Health'
    TASK_LEVEL = os.environ['PREFIX_TASK_LEVEL']
    BILLING_MONTH = 'BillingMonth'


class IssueLabel(Enum):
    STATUS_BACKLOG = f'{IssueLabelPrefix.STATUS.value}::Backlog'
    STATUS_WIP = f'{IssueLabelPrefix.STATUS.value}::WIP'
    STATUS_REVIEW = f'{IssueLabelPrefix.STATUS.value}::Review'
    STATUS_DONE = f'{IssueLabelPrefix.STATUS.value}::Done'
    HEALTH_ON_TRACK = f'{IssueLabelPrefix.HEALTH.value}::On Track'
    HEALTH_NEEDS_ATTENTION = f'{
        IssueLabelPrefix.HEALTH.value}::Needs Attention'
    HEALTH_AT_RISK = f'{IssueLabelPrefix.HEALTH.value}::At Risk'
    TASK_LEVEL_100 = f'{IssueLabelPrefix.TASK_LEVEL.value}::100'
    TASK_LEVEL_200 = f'{IssueLabelPrefix.TASK_LEVEL.value}::200'
    # BillingMonth::%Y-%mがあるが、動的なものなので定義不可


class EventBridgeDetailType(Enum):
    ADD_LABEL = 'add label'
    SET_ASSIGNEES = 'set assignees'


class GitLabIssueEbSchedulerGroup(Enum):
    HEALTH_NEEDS_ATTENTION = os.environ['GITLAB_ISSUE_HEALTH_NEEDS_ATTENTION_EB_SCHEDULER_GROUP']
    HEALTH_AT_RISK = os.environ['GITLAB_ISSUE_HEALTH_AT_RISK_EB_SCHEDULER_GROUP']
