import base64
from datetime import datetime, timedelta
import json
import os
import time
from zoneinfo import ZoneInfo
from dataclasses import asdict

import requests
import yaml
from enums import (
    ObjectKind, IssueState, IssueLabel,
    IssueLabelPrefix, EventBridgeDetailType, LabelChangeState,
    GitLabIssueEbSchedulerGroup, EventAction
)
from models import (IssueDetail, EventBridgeAddLabelDetail, EventBridgeSetAssigneesDetail,
                    MergeRequestDetail, GitLabFileResponse
                    )
from utils import enum_to_str

import boto3
eventbridge = boto3.client('events')
scheduler = boto3.client('scheduler')
secrets_manager = boto3.client('secretsmanager')

JST = ZoneInfo('Asia/Tokyo')
CONFIG_FILE_PATH = '.gitlab%2Fissue_manager.yaml'
EVENT_BRIDGE_EVENT_SOURCE = 'gitlab.com'
TASK_LEVEL_200_USER = 'user200'
STATUS_LABEL_PREFIX = 'Status::'
GITLAB_API_BASE_URL = 'https://gitlab.com/api/v4'
EVENT_BUS = os.environ['EVENT_BUS']
EVENT_BUS_ARN = os.environ['EVENT_BUS_ARN']
EVENT_BRIDGE_SCHEDULER_ROLE_ARN = os.environ['EVENT_BRIDGE_SCHEDULER_ROLE_ARN']
GIT_PATH = os.environ['GIT_PATH']
GITLAB_GROUP_ACCESS_TOKEN_SECRET_ARN = os.environ['GITLAB_GROUP_ACCESS_TOKEN_SECRET_ARN']

WEEKDAY_TO_SCHEDULE_OFFSET = {
    0: 2,  # 月
    1: 2,  # 火
    2: 2,  # 水
    3: 4,  # 木
    4: 4,  # 金
    5: 4,  # 土
    6: 3,  # 日
}


def lambda_handler(event, context):
    # TODO: 命名が悪い。issueだけではなMRなどもあるので、ここで断定するべきではない。
    issue_body = json.loads(event['body'])
    print(issue_body)
    config_response = load_file_content_from_gitlab_repository(
        issue_body['project']['id'], CONFIG_FILE_PATH)
    if config_response and config_response.status == 404:
        return create_lambda_response(200, {'message': config_response.message})
    print(config_response)
    match ObjectKind(issue_body.get('object_kind')):
        case ObjectKind.ISSUE:
            issue_detail = IssueDetail.from_issue_body(issue_body)
            handle_closed_issue(issue_body, issue_detail)
            handle_label_changes(issue_body, issue_detail)
        case ObjectKind.MERGE_REQUEST:
            mr_detail = MergeRequestDetail.from_mr_body(issue_body)
            issue_list = get_issues_closed_by_mr(
                mr_detail.project_id, mr_detail.iid)
            if len(issue_list) > 0:
                handle_mr_open_action(mr_detail, issue_list)
                handle_mr_update_action(mr_detail, issue_list)

    return create_lambda_response(200, {'message': 'success'})


def create_lambda_response(status_code, body):
    return {
        "statusCode": status_code,
        "body": json.dumps(body, ensure_ascii=False),
    }


def load_file_content_from_gitlab_repository(project_id: int, file_path: str):
    # /projects/{project_id}/repository/files/{file_path}?ref=mainのgitlab apiを実行して指定したファイルのcontentをロードする関数
    url = GITLAB_API_BASE_URL + \
        f'/projects/{project_id}/repository/files/{file_path}?ref=main'
    header = get_gitlab_api_headers()
    response = requests.get(url, headers=header)
    match response.status_code:
        case 200:
            b64encode_content = response.json()['content']
            str_content = base64.b64decode(b64encode_content).decode('utf-8')
            content = yaml.safe_load(str_content)
            return GitLabFileResponse(
                status=response.status_code,
                content=content,
            )
        case 404:
            return GitLabFileResponse(
                status=response.status_code,
                message=f'{file_path}が見つからなかったので、処理は実行されませんでした。'
            )
        case _:
            response.raise_for_status()


def handle_label_changes(issue_body, issue_detail):
    if 'labels' in issue_body['changes']:
        check_and_assign_active_task(issue_detail, issue_body)
        check_and_add_billing_month_label(issue_detail, issue_body)
        check_and_add_task_level_label(issue_body, issue_detail)
        check_and_add_health_on_track_label_and_schedule(
            issue_body, issue_detail)
        check_and_add_health_needs_attention_label_and_schedule(
            issue_body, issue_detail)
        check_leave_wip_and_reset_health_label(issue_body, issue_detail)


def handle_closed_issue(issue_body, issue_detail):
    # closeのステータスになっているissue。closeされた瞬間のイベントを判断しているわけではない。その瞬間を判断するのはactionプロパティ
    if issue_detail.state == IssueState.CLOSED:
        add_done_label_if_not_exist(issue_body, issue_detail)


def handle_mr_open_action(mr_detail: MergeRequestDetail, issue_list: list):
    if mr_detail.action == EventAction.OPEN:
        assign_user_and_add_wip_if_backlog_exists(mr_detail, issue_list)


def handle_mr_update_action(mr_detail: MergeRequestDetail, issue_list: list):
    if mr_detail.action == EventAction.UPDATE:
        check_and_add_review_label(mr_detail, issue_list)


def add_done_label_if_not_exist(issue_body, issue_detail):
    if not have_label_check_from_dict_labels(issue_body['labels'], IssueLabel.STATUS_DONE):
        detail = EventBridgeAddLabelDetail(
            project_id=issue_detail.project_id,
            iid=issue_detail.iid,
            add_label=IssueLabel.STATUS_DONE,
        )
        add_label(detail)


def have_label_check_from_dict_labels(labels: list[dict], target_label: IssueLabel | IssueLabelPrefix, startswith: bool = False) -> bool:
    _labels: list[str] = [label['title'] for label in labels]
    return have_label_check(_labels, target_label, startswith)


def have_label_check(labels: list[str], target_label: IssueLabel | IssueLabelPrefix, startswith: bool = False) -> bool:
    if startswith:
        return any(label.startswith(target_label.value) for label in labels)
    return target_label.value in labels


def add_label(detail: EventBridgeAddLabelDetail):
    detail_dict = asdict(detail)

    eventbridge.put_events(
        Entries=[
            {
                # TODO: 厳密には下記のSourceはおかしいよね？
                'Source': EVENT_BRIDGE_EVENT_SOURCE,
                'DetailType': EventBridgeDetailType.ADD_LABEL.value,
                'Detail': json.dumps(detail_dict, default=enum_to_str),
                'EventBusName': EVENT_BUS,
            }
        ]
    )


def check_and_assign_active_task(issue_detail: IssueDetail, issue_body: dict):
    # 未アサインでBacklogから別のステータスラベルに移動した場合、ユーザーをアサインする
    if not issue_detail.username:
        if was_previously_in_backlog(issue_body) and is_currently_not_in_backlog(issue_body):
            set_assignees(
                EventBridgeSetAssigneesDetail(
                    issue_detail.project_id,
                    issue_detail.iid,
                    issue_detail.user_id,
                )
            )


def extract_prefix_from_label(label: IssueLabel) -> str:
    prefix_end_index = label.value.find('::') + 2
    return label.value[:prefix_end_index]


def exist_in_changes_labels(issue_body: dict, label: IssueLabel, label_change_state: LabelChangeState) -> bool:
    return any(
        item['title'] == label.value
        for item in issue_body['changes']['labels'].get(label_change_state.value, [])
    )


def not_exist_in_changes_labels(issue_body: dict, label: IssueLabel, label_change_state: LabelChangeState) -> bool:
    label_prefix = extract_prefix_from_label(label)
    return any(
        item['title'] != label.value
        and item['title'].startswith(label_prefix)
        for item in issue_body['changes']['labels'].get(label_change_state.value, [])
    )


def was_previously_in_backlog(issue_body: dict) -> bool:
    return exist_in_changes_labels(issue_body, IssueLabel.STATUS_BACKLOG, LabelChangeState.PREVIOUS)


def is_currently_not_in_backlog(issue_body: dict) -> bool:
    return any(
        current_item['title'].startswith(STATUS_LABEL_PREFIX)
        and current_item['title'] != IssueLabel.STATUS_BACKLOG.value
        for current_item in issue_body['changes']['labels'].get('current', [])
    )


def set_assignees(detail: EventBridgeSetAssigneesDetail):
    detail_dict = asdict(detail)
    eventbridge.put_events(
        Entries=[
            {
                'Source': EVENT_BRIDGE_EVENT_SOURCE,
                'DetailType': EventBridgeDetailType.SET_ASSIGNEES.value,
                'Detail': json.dumps(detail_dict),
                'EventBusName': EVENT_BUS,
            },
        ],
    )


def check_and_add_billing_month_label(issue_detail: IssueDetail, issue_body: dict):

    if (issue_detail.git_path.startswith(GIT_PATH)
            and check_done(issue_body, issue_detail, LabelChangeState.CURRENT)
            and check_not_done(issue_body, LabelChangeState.PREVIOUS)):
        now = datetime.now(JST)
        label = now.strftime('BillingMonth::%Y-%m')
        add_label(EventBridgeAddLabelDetail(
            project_id=issue_detail.project_id,
            iid=issue_detail.iid,
            add_label=label,
        ))


def check_done(issue_body: dict, issue_detail: IssueDetail, label_change_state: LabelChangeState):
    return (issue_detail.state == IssueState.CLOSED
            or exist_in_changes_labels(issue_body, IssueLabel.STATUS_DONE, label_change_state))


def check_not_done(issue_body: dict, label_change_state: LabelChangeState):
    return not_exist_in_changes_labels(issue_body, IssueLabel.STATUS_DONE, label_change_state)


def check_and_add_health_on_track_label_and_schedule(issue_body: dict, issue_detail: IssueDetail):
    if check_wip(issue_body, LabelChangeState.CURRENT) and check_not_wip(issue_body, LabelChangeState.PREVIOUS):
        add_label(EventBridgeAddLabelDetail(
            project_id=issue_body['project']['id'],
            iid=issue_body['object_attributes']['iid'],
            add_label=IssueLabel.HEALTH_ON_TRACK,
        ))
        set_schedule(
            EventBridgeAddLabelDetail(
                issue_detail.project_id,
                issue_detail.iid,
                IssueLabel.HEALTH_NEEDS_ATTENTION,
            ),
            GitLabIssueEbSchedulerGroup.HEALTH_NEEDS_ATTENTION,
        )


def check_wip(issue_body: dict, label_change_state: LabelChangeState):
    return exist_in_changes_labels(issue_body, IssueLabel.STATUS_WIP, label_change_state)


def check_not_wip(issue_body: dict, label_change_state: LabelChangeState):
    return not_exist_in_changes_labels(issue_body, IssueLabel.STATUS_WIP, label_change_state)


def check_schedule_exists(project_id: int, iid: int, group_name: GitLabIssueEbSchedulerGroup) -> bool:
    response = scheduler.list_schedules(
        GroupName=group_name.value,
        NamePrefix=f'{project_id}_{iid}',
    )
    return len(response['Schedules']) > 0


def calculate_schedule_date() -> datetime:
    now = datetime.now(JST)
    weekday = now.weekday()
    schedule_date = now + timedelta(days=WEEKDAY_TO_SCHEDULE_OFFSET[weekday])
    return schedule_date


def set_schedule(
        detail: EventBridgeAddLabelDetail, group_name: GitLabIssueEbSchedulerGroup):
    # if not isinstance(group_name, GitLabIssueEbSchedulerGroup):
    #     raise ValueError("Invalid group_name parameter")
    schedule_exists = check_schedule_exists(
        detail.project_id,
        detail.iid,
        group_name,
    )
    schedule_date = calculate_schedule_date()
    input = asdict(detail)
    param_schdule = {
        'FlexibleTimeWindow': {
            'MaximumWindowInMinutes': 15,
            'Mode': 'FLEXIBLE',
        },
        'GroupName': group_name.value,
        'Name': f"{detail.project_id}_{detail.iid}",
        'ScheduleExpression': schedule_date.strftime('at(%Y-%m-%dT%H:%M:%S)'),
        'ScheduleExpressionTimezone': 'Asia/Tokyo',
        'State': 'ENABLED',
        'Target': {
            'Arn': EVENT_BUS_ARN,
            'EventBridgeParameters': {
                'DetailType': EventBridgeDetailType.ADD_LABEL.value,
                'Source': EVENT_BRIDGE_EVENT_SOURCE,
            },
            'Input': json.dumps(input, default=enum_to_str),
            'RoleArn': EVENT_BRIDGE_SCHEDULER_ROLE_ARN,
        }
    }
    if schedule_exists:
        scheduler.update_schedule(**param_schdule)
    else:
        scheduler.create_schedule(**param_schdule)


def check_and_add_task_level_label(issue_body: dict, issue_detail: IssueDetail):
    if issue_detail.git_path.startswith(GIT_PATH):
        # 今がdoneでtask_levelラベルが未設定でアサイン済みの場合
        if (
            check_done(issue_body, issue_detail, LabelChangeState.CURRENT)
                and not have_label_check_from_dict_labels(
                    issue_body.get('labels', {}),
                    IssueLabelPrefix.TASK_LEVEL,
                    startswith=True)
                and issue_detail.username):
            if issue_detail.username == TASK_LEVEL_200_USER:
                label = IssueLabel.TASK_LEVEL_200
            else:
                label = IssueLabel.TASK_LEVEL_100
            add_label(EventBridgeAddLabelDetail(
                issue_detail.project_id,
                issue_detail.iid,
                add_label=label,
            ))


def check_leave_wip_and_reset_health_label(issue_body: dict, issue_detail: IssueDetail):
    if (
        check_wip(issue_body, LabelChangeState.PREVIOUS)
        and check_not_wip(issue_body, LabelChangeState.CURRENT)
    ):
        delete_schedule_if_exist(
            issue_detail, GitLabIssueEbSchedulerGroup.HEALTH_NEEDS_ATTENTION)
        delete_schedule_if_exist(
            issue_detail, GitLabIssueEbSchedulerGroup.HEALTH_AT_RISK)
        add_label(EventBridgeAddLabelDetail(
            issue_detail.project_id,
            issue_detail.iid,
            add_label=IssueLabel.HEALTH_ON_TRACK,
        ))


def delete_schedule_if_exist(issue_detail: IssueDetail, group_name: GitLabIssueEbSchedulerGroup):
    if check_schedule_exists(
            issue_detail.project_id,
            issue_detail.iid,
            group_name,
    ):
        scheduler.delete_schedule(
            GroupName=group_name.value,
            Name=f"{issue_detail.project_id}_{issue_detail.iid}",

        )


def check_and_add_health_needs_attention_label_and_schedule(issue_body: dict, issue_detail: IssueDetail):
    if (
        check_wip(issue_body, LabelChangeState.PREVIOUS)
        and check_wip(issue_body, LabelChangeState.CURRENT)
        and check_on_track(issue_body, LabelChangeState.PREVIOUS)
        and check_needs_attention(issue_body, LabelChangeState.CURRENT)
    ):
        delete_schedule_if_exist(
            issue_detail, GitLabIssueEbSchedulerGroup.HEALTH_NEEDS_ATTENTION)
        set_schedule(
            EventBridgeAddLabelDetail(
                issue_detail.project_id,
                issue_detail.iid,
                IssueLabel.HEALTH_AT_RISK,
            ),
            GitLabIssueEbSchedulerGroup.HEALTH_AT_RISK,
        )


def check_on_track(issue_body: dict, label_change_state: LabelChangeState):
    return exist_in_changes_labels(issue_body, IssueLabel.HEALTH_ON_TRACK, label_change_state)


def check_needs_attention(issue_body: dict, label_change_state: LabelChangeState):
    return exist_in_changes_labels(issue_body, IssueLabel.HEALTH_NEEDS_ATTENTION, label_change_state)


def get_issues_closed_by_mr(project_id: int, iid: int) -> list:
    url = f"{GITLAB_API_BASE_URL}/projects/{
        project_id}/merge_requests/{iid}/closes_issues"
    headers = get_gitlab_api_headers()
    response = requests.get(url, headers=headers)
    response.raise_for_status()
    return response.json()


def assign_user_and_add_wip_if_backlog_exists(mr_detail: MergeRequestDetail, issue_list: list):
    for issue in issue_list:
        if have_label_check(issue['labels'], IssueLabel.STATUS_BACKLOG):
            set_assignees(EventBridgeSetAssigneesDetail(
                project_id=mr_detail.project_id,
                iid=issue['iid'],
                user_id=mr_detail.author_id,
            ))
            time.sleep(1)
            add_label(EventBridgeAddLabelDetail(
                project_id=mr_detail.project_id,
                iid=issue['iid'],
                add_label=IssueLabel.STATUS_WIP,
            ))


def check_and_add_review_label(mr_detail: MergeRequestDetail, issue_list: list):
    for issue in issue_list:
        if have_label_check(issue['labels'], IssueLabel.STATUS_WIP):
            if mr_detail.changes.get('draft') and not mr_detail.changes['draft'].get('current'):
                add_label(EventBridgeAddLabelDetail(
                    project_id=mr_detail.project_id,
                    iid=issue['iid'],
                    add_label=IssueLabel.STATUS_REVIEW,
                ))


def get_gitlab_api_headers() -> dict:
    # シークレットマネージャーからgitlabのトークンを取得して、gitlab apiのヘッダーとして整形して返す
    # .rsplit('-', 1)[0]の処理はArnとIdの命名規則が明確に確認できていないので要注意
    secret_id = os.environ['GITLAB_GROUP_ACCESS_TOKEN_SECRET_ARN'].split(
        ':')[-1].rsplit('-', 1)[0]
    gitlab_token = json.loads(secrets_manager.get_secret_value(
        SecretId=secret_id)['SecretString'])['api_key_value']
    return {'PRIVATE-TOKEN': gitlab_token}
