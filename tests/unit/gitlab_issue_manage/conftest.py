import os
import pytest
from pytest import MonkeyPatch
from moto import mock_aws
import boto3
from freezegun import freeze_time

# インポート順を制御するのが手間なので、強引に環境変数をセットする
os.environ['GITLAB_ISSUE_HEALTH_NEEDS_ATTENTION_EB_SCHEDULER_GROUP'] = 'GITLAB_ISSUE_HEALTH_NEEDS_ATTENTION_EB_SCHEDULER_GROUP'
os.environ['GITLAB_ISSUE_HEALTH_AT_RISK_EB_SCHEDULER_GROUP'] = 'GITLAB_ISSUE_HEALTH_AT_RISK_EB_SCHEDULER_GROUP'
os.environ['PREFIX_TASK_LEVEL'] = 'PREFIX_TASK_LEVEL'

aws_moto_env = {
    'AWS_ACCESS_KEY_ID': 'testing',
    'AWS_SECRET_ACCESS_KEY': 'testing',
    'AWS_SECURITY_TOKEN': 'testing',
    'AWS_SESSION_TOKEN': 'testing',
    'AWS_SESSION_TOKEN': 'testing',
    'AWS_DEFAULT_REGION': 'us-east-1'
}


@pytest.fixture(autouse=True)
def setenv(monkeypatch: MonkeyPatch):
    env = {
        'EVENT_BUS': 'EVENT_BUS',
        'EVENT_BUS_ARN': 'EVENT_BUS_ARN',
        'EVENT_BRIDGE_SCHEDULER_ROLE_ARN': 'EVENT_BRIDGE_SCHEDULER_ROLE_ARN',
        'GIT_PATH': 'user123/project-name',
        'GITLAB_GROUP_ACCESS_TOKEN_SECRET_ARN': 'arn:aws:secretsmanager:region:123456789012:secret:id/foo/bar-hoge-fuga',
    } | aws_moto_env

    for k, v in env.items():
        monkeypatch.setenv(k, v)


@pytest.fixture(autouse=True)
def freeze_time_fixture():
    with freeze_time('2024-04-01', -9):
        yield


@pytest.fixture(autouse=True)
def moto_mock():
    with mock_aws():
        yield


@pytest.fixture
def init_eventbridge(moto_mock):
    client = boto3.client('events')
    client.create_event_bus(Name='EVENT_BUS')


@pytest.fixture
def init_scheduler(moto_mock):
    from gitlab_issue_manage.enums import GitLabIssueEbSchedulerGroup
    client = boto3.client('scheduler')
    client.create_schedule_group(
        Name=GitLabIssueEbSchedulerGroup.HEALTH_NEEDS_ATTENTION.value)
    client.create_schedule_group(
        Name=GitLabIssueEbSchedulerGroup.HEALTH_AT_RISK.value)


@pytest.fixture
def init_secret_manager(moto_mock):
    secret_id = os.environ['GITLAB_GROUP_ACCESS_TOKEN_SECRET_ARN'].split(
        ':')[-1].rsplit('-', 1)[0]
    client = boto3.client('secretsmanager')
    client.create_secret(
        Name=secret_id,
        SecretString='{"api_key_name":"PRIVATE-TOKEN","api_key_value":"test"}'
    )
