import json
import pytest
from gitlab_issue_manage import utils, enums


def test_enum_to_str():
    assert utils.enum_to_str(enums.IssueState.OPEN) == "opened"
    assert utils.enum_to_str(enums.IssueState.CLOSED) == "closed"


def test_enum_to_str_invalid():
    with pytest.raises(TypeError) as e:
        utils.enum_to_str("invalid")
    assert e.value.args[0] == "Object of type str is not JSON serializable"


def test_enum_to_str_for_json_dumps_arg_default():
    input = {'a': 'a', 'state': enums.IssueState.OPEN}
    res = json.dumps(input, default=utils.enum_to_str)
    assert res == '{"a": "a", "state": "opened"}'
