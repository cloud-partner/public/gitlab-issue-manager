import pytest
from gitlab_issue_manage import enums


@pytest.mark.parametrize('target_enum', [enums.ObjectKind, enums.EventAction, enums.IssueState])
def test_enum_with_unknown_value(target_enum):
    res = target_enum('hoge')
    assert res == target_enum.UNKNOWN, f"Expected UNKNOWN, but got {res}"
