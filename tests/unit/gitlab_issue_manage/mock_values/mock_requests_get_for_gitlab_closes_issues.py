mock_value = [
    {
        "id": 123456789,
        "iid": 1,
        "project_id": 12345678,
        "title": "demo1",
        "description": "",
        "state": "opened",
        "created_at": "2024-05-09T04:13:15.771Z",
        "updated_at": "2024-05-17T07:36:01.908Z",
        "closed_at": None,
        "closed_by": None,
        "labels": ["Status::Backlog", "b1"],
        "milestone": None,
        "assignees": [
            {
                "id": 123456,
                "username": "user123",
                "name": "XXXX",
                "state": "active",
                "locked": False,
                "avatar_url": "https://secure.gravatar.com/avatar/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx?s=80&d=identicon",
                "web_url": "https://gitlab.com/user123"
            }
        ],
        "author": {
            "id": 123456,
            "username": "user123",
            "name": "XXXX",
            "state": "active",
            "locked": False,
            "avatar_url": "https://secure.gravatar.com/avatar/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx?s=80&d=identicon",
            "web_url": "https://gitlab.com/user123"
        },
        "type": "ISSUE",
        "assignee": {
            "id": 123456,
            "username": "user123",
            "name": "XXXX",
            "state": "active",
            "locked": False,
            "avatar_url": "https://secure.gravatar.com/avatar/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx?s=80&d=identicon",
            "web_url": "https://gitlab.com/user123"
        },
        "user_notes_count": 0,
        "merge_requests_count": 1,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": None,
        "confidential": False,
        "discussion_locked": None,
        "issue_type": "issue",
        "web_url": "https://gitlab.com/user123/project-name/-/issues/1",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": None,
            "human_total_time_spent": None
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "blocking_issues_count": 0
    },
    {
        "id": 123456790,
        "iid": 6,
        "project_id": 12345678,
        "title": "2024-05-24-demo1",
        "description": "",
        "state": "opened",
        "created_at": "2024-05-24T05:01:17.432Z",
        "updated_at": "2024-05-29T03:51:23.056Z",
        "closed_at": None,
        "closed_by": None,
        "labels": ["Health::On Track", "Status::WIP"],
        "milestone": None,
        "assignees": [
            {
                "id": 123456,
                "username": "user123",
                "name": "XXXX",
                "state": "active",
                "locked": False,
                "avatar_url": "https://secure.gravatar.com/avatar/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx?s=80&d=identicon",
                "web_url": "https://gitlab.com/user123"
            }
        ],
        "author": {
            "id": 123456,
            "username": "user123",
            "name": "XXXX",
            "state": "active",
            "locked": False,
            "avatar_url": "https://secure.gravatar.com/avatar/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx?s=80&d=identicon",
            "web_url": "https://gitlab.com/user123"
        },
        "type": "ISSUE",
        "assignee": {
            "id": 123456,
            "username": "user123",
            "name": "XXXX",
            "state": "active",
            "locked": False,
            "avatar_url": "https://secure.gravatar.com/avatar/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx?s=80&d=identicon",
            "web_url": "https://gitlab.com/user123"
        },
        "user_notes_count": 0,
        "merge_requests_count": 1,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": None,
        "confidential": False,
        "discussion_locked": None,
        "issue_type": "issue",
        "web_url": "https://gitlab.com/user123/project-name/-/issues/6",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": None,
            "human_total_time_spent": None
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "blocking_issues_count": 0
    }
]
