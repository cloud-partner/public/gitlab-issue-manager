mock_value = {
    "file_name": "issue_manager.yaml",
    "file_path": ".gitlab/issue_manager.yaml",
    "size": 30,
    "encoding": "base64",
    "content_sha256": "06a6308b51767b2d72fe360225a9757f573691d52a5af379a8f24fab683f0016",
    "ref": "main",
    "blob_id": "cbfdddb0aadfe0654e27ae6270b64ce6852f7a1c",
    "commit_id": "af044ab7c8a037cbfccf3ba424bc61f890c4adf2",
    "last_commit_id": "af044ab7c8a037cbfccf3ba424bc61f890c4adf2",
    "execute_filemode": False,
    "content": "aG9nZToKICAgIGZ1Z2E6IGZvbwogICAgYmFyOiB4"
}
