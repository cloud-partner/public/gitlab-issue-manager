from datetime import datetime
from zoneinfo import ZoneInfo
import json
import os
import boto3
import pytest
from pytest_mock import MockerFixture
from freezegun import freeze_time
from pathlib import Path
# このパスでインポートすると、app.pyのものとは別物になるからあまりよくない
from gitlab_issue_manage.enums import (
    IssueLabel, EventBridgeDetailType, LabelChangeState,
    GitLabIssueEbSchedulerGroup, IssueState
)
from gitlab_issue_manage.models import (
    EventBridgeAddLabelDetail, EventBridgeSetAssigneesDetail,
    IssueDetail, MergeRequestDetail
)
from tests.unit.gitlab_issue_manage.mock_values.mock_requests_get_for_gitlab_closes_issues \
    import mock_value as mock_value_requests_get_for_gitlab_closes_issues
from tests.unit.gitlab_issue_manage.mock_values.mock_load_file_content_from_gitlab_repository \
    import mock_value as mock_value_load_file_content_from_gitlab_repository

JST = ZoneInfo('Asia/Tokyo')


@pytest.fixture
def app():
    from gitlab_issue_manage import app
    return app


class MockResponce:
    def __init__(self, mock_value, status_code=200):
        self.mock_value = mock_value
        self.status_code = status_code

    def json(self):
        return self.mock_value

    def raise_for_status(self):
        pass


@pytest.fixture
def mock_requests_get_for_gitlab_closes_issues(mocker: MockerFixture):
    mock = mocker.patch('requests.get')
    mock.return_value = MockResponce(
        mock_value_requests_get_for_gitlab_closes_issues)
    return mock


def set_event_body(event):
    return {
        "body": event.read()
    }


@pytest.mark.parametrize('event_file, mock_get_config_file_status_code', [
    ('issue_close.json', 200),
    ('issue_backlog_to_wip.json', 200),
    ('mr_open_action.json', 200),
    ('mr_open_action.json', 404),
])
def test_lambda_handler(
    mocker: MockerFixture, event_file, mock_get_config_file_status_code,
    init_eventbridge, init_scheduler, init_secret_manager,
        app
):
    mock = mocker.patch('requests.get')
    mock.side_effect = [
        MockResponce(
            mock_value_load_file_content_from_gitlab_repository, mock_get_config_file_status_code),
        MockResponce(mock_value_requests_get_for_gitlab_closes_issues),
    ]
    file_path = Path('events') / event_file
    with open(file_path, 'r') as f:
        event = set_event_body(f)
    res = app.lambda_handler(event, {})
    assert res['statusCode'] == 200


def test_create_lambda_response(app):
    res = app.create_lambda_response(200, {'message': 'test'})
    assert res == {
        'statusCode': 200,
        'body': '{"message": "test"}'
    }


class Test_load_file_content_from_gitlab_repository:
    def test_200_status_code(self, mocker: MockerFixture, init_secret_manager, app):
        mock = mocker.patch('requests.get')
        mock.return_value = MockResponce(
            mock_value_load_file_content_from_gitlab_repository, 200)
        res = app.load_file_content_from_gitlab_repository(123, 'test.yaml')
        assert res == app.GitLabFileResponse(
            200, content={'hoge': {'fuga': 'foo', 'bar': 'x'}})
        mock.assert_called_once_with(
            'https://gitlab.com/api/v4/projects/123/repository/files/test.yaml?ref=main',
            headers={'PRIVATE-TOKEN': 'test'}
        )

    def test_404_status_code(self, mocker: MockerFixture, init_secret_manager, app):
        mock = mocker.patch('requests.get')
        mock.return_value = MockResponce({}, 404)
        res = app.load_file_content_from_gitlab_repository(123, 'test.yaml')
        assert res == app.GitLabFileResponse(
            404,
            message='test.yamlが見つからなかったので、処理は実行されませんでした。')

    def test_500_status_code(self, mocker: MockerFixture, init_secret_manager, app):
        mock = mocker.patch('requests.get')
        mock.status_code.return_value = 500
        res = app.load_file_content_from_gitlab_repository(123, 'test.yaml')
        assert res is None


@pytest.mark.parametrize('file_name,is_called', [
    ('issue_backlog_to_wip.json', True),
    ('issue_close.json', False),
])
def test_handle_label_changes(mocker: MockerFixture, file_name, is_called, init_eventbridge, init_scheduler, app):
    mock_check_and_assign_active_task = mocker.patch.object(
        app, 'check_and_assign_active_task')
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    issue_detail = IssueDetail.from_issue_body(issue_body)
    app.handle_label_changes(
        issue_body,
        issue_detail,
    )
    if is_called:
        mock_check_and_assign_active_task.assert_called_once()
    else:
        mock_check_and_assign_active_task.assert_not_called()


@pytest.mark.parametrize('file_name,is_called', [
    ('issue_close.json', True),
    ('issue_backlog_to_wip.json', False),
])
def test_handle_closed_issue(mocker: MockerFixture, file_name, is_called, app):
    mock_add_done_label_if_not_exist = mocker.patch.object(
        app, 'add_done_label_if_not_exist')
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    issue_detail = IssueDetail.from_issue_body(issue_body)
    app.handle_closed_issue(
        issue_body,
        issue_detail,
    )
    if is_called:
        mock_add_done_label_if_not_exist.assert_called_once()
    else:
        mock_add_done_label_if_not_exist.assert_not_called()


def test_handle_mr_open_action(mocker: MockerFixture, init_eventbridge, app):
    spy = mocker.spy(app, 'assign_user_and_add_wip_if_backlog_exists')
    issue_list = mock_value_requests_get_for_gitlab_closes_issues
    app.handle_mr_open_action(
        MergeRequestDetail(123, 1, '', app.EventAction.OPEN, 456, {}),
        issue_list,
    )
    spy.assert_called_once()


def test_handle_mr_update_action(mocker: MockerFixture, init_eventbridge, app):
    spy = mocker.spy(app, 'check_and_add_review_label')
    issue_list = mock_value_requests_get_for_gitlab_closes_issues
    app.handle_mr_update_action(
        MergeRequestDetail(
            123, 1, '', app.EventAction.UPDATE, 456,
            {
                'draft': {'current': False}
            }
        ),
        issue_list,
    )
    spy.assert_called_once()


@pytest.mark.parametrize('file_name,is_called', [
    ('issue_close.json', True),
    ('issue_close_already_done_label.json', False),
])
def test_add_done_label_if_not_exist(mocker: MockerFixture, file_name, is_called, app):
    mock_add_label = mocker.patch.object(
        app, 'add_label')
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    issue_detail = IssueDetail.from_issue_body(issue_body)
    app.add_done_label_if_not_exist(
        issue_body,
        issue_detail,
    )
    if is_called:
        mock_add_label.assert_called_once()
    else:
        mock_add_label.assert_not_called()


@pytest.mark.parametrize(
    'input,expected',
    [
        (
            (
                [{'title': IssueLabel.STATUS_DONE.value, 'a': 'a'}, {'title': 'hoge'}],
                IssueLabel.STATUS_DONE
            ),
            True
        ),
        (
            (
                [{'title': 'hoge', 'a': 'a'}, {'title': 'fuga', 'a': 'a'},],
                IssueLabel.STATUS_WIP
            ),
            False
        ),
    ]
)
def test_have_label_check_from_dict_labels(input, expected, app):
    assert app.have_label_check_from_dict_labels(*input) == expected


@pytest.mark.parametrize(
    'input,expected',
    [
        (
            (
                ['hoge', 'fuga', IssueLabel.STATUS_BACKLOG.value],
                IssueLabel.STATUS_BACKLOG
            ),
            True
        ),
        (
            (
                ['hoge', 'fuga', IssueLabel.STATUS_BACKLOG.value],
                IssueLabel.STATUS_WIP
            ),
            False
        ),
    ]
)
def test_have_label_check(input, expected, app):
    assert app.have_label_check(*input) == expected


def test_add_label(mocker: MockerFixture, init_eventbridge, app):
    spy = mocker.spy(app.eventbridge, 'put_events')
    app.add_label(EventBridgeAddLabelDetail(
        12345,
        1,
        IssueLabel.STATUS_DONE,
    ))
    spy.assert_called_once_with(
        Entries=[
            {
                'Source': app.EVENT_BRIDGE_EVENT_SOURCE,
                'DetailType': EventBridgeDetailType.ADD_LABEL.value,
                'Detail': '{"project_id": 12345, "iid": 1, "add_label": "Status::Done"}',
                'EventBusName': app.EVENT_BUS,
            }
        ]
    )


@pytest.mark.parametrize('file_name,is_called', [
    ('issue_backlog_to_wip.json', True),
    ('issue_assigned_and_moved_from_backlog.json', False),
    ('issue_label_change.json', False),
])
def test_check_and_assign_active_task(mocker: MockerFixture, file_name, is_called, init_eventbridge, app):
    spy = mocker.spy(app, 'set_assignees')
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    issue_detail = IssueDetail.from_issue_body(issue_body)
    app.check_and_assign_active_task(
        issue_detail,
        issue_body,
    )
    if is_called:
        spy.assert_called_once()
    else:
        spy.assert_not_called()


def test_extract_prefix_from_label(app):
    assert app.extract_prefix_from_label(
        IssueLabel.STATUS_BACKLOG) == 'Status::'


def test_exist_in_changes_labels(app):
    file_path = Path('events') / 'issue_backlog_to_wip.json'
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    assert app.exist_in_changes_labels(
        issue_body, IssueLabel.STATUS_BACKLOG, LabelChangeState.PREVIOUS
    ) is True
    assert app.exist_in_changes_labels(
        issue_body, IssueLabel.STATUS_BACKLOG, LabelChangeState.CURRENT
    ) is False
    assert app.exist_in_changes_labels(
        issue_body, IssueLabel.STATUS_WIP, LabelChangeState.PREVIOUS
    ) is False
    assert app.exist_in_changes_labels(
        issue_body, IssueLabel.STATUS_WIP, LabelChangeState.CURRENT
    ) is True


def test_not_exist_in_changes_labels(app):
    file_path = Path('events') / 'issue_backlog_to_wip.json'
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    assert app.not_exist_in_changes_labels(
        issue_body, IssueLabel.STATUS_WIP, LabelChangeState.PREVIOUS
    ) is True
    assert app.not_exist_in_changes_labels(
        issue_body, IssueLabel.STATUS_BACKLOG, LabelChangeState.PREVIOUS
    ) is False


@pytest.mark.parametrize('file_name,expected', [
    ('issue_backlog_to_wip.json', True),
    ('issue_label_change.json', False),
])
def test_was_previously_in_backlog(file_name, expected, app):
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    assert app.was_previously_in_backlog(issue_body) == expected


@pytest.mark.parametrize('file_name,expected', [
    ('issue_backlog_to_wip.json', True),
    ('issue_backlog_removal.json', False),
    ('issue_label_change.json', False),
])
def test_is_currently_not_in_backlog(file_name, expected, app):
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    assert app.is_currently_not_in_backlog(issue_body) == expected


def test_set_assignees(mocker: MockerFixture, init_eventbridge, app):
    spy = mocker.spy(app.eventbridge, 'put_events')
    app.set_assignees(EventBridgeSetAssigneesDetail(
        12345,
        1,
        67890,
    ))
    spy.assert_called_once_with(
        Entries=[
            {
                'Source': app.EVENT_BRIDGE_EVENT_SOURCE,
                'DetailType': EventBridgeDetailType.SET_ASSIGNEES.value,
                'Detail': '{"project_id": 12345, "iid": 1, "user_id": 67890}',
                'EventBusName': app.EVENT_BUS,
            }
        ]
    )


@pytest.mark.parametrize('file_name,is_called', [
    ('issue_wip_to_done.json', True),
    # 特殊なオペレーションしないとほぼ実現しないイベントパターン（closeしながらラベルを変更）
    ('issue_closed_state_change_label.json', True),
    ('issue_label_change.json', False),
    # TODO: 本来ならばBillingMonthをセットしたいパターンだが、check_and_add_billing_month_labelだとラベルの変更が前提条件なんで不可能。なので、actionがcloseのときの条件分岐で実装する必要あり。
    # ('issue_close_action_not_change_label.json', False),
])
def test_check_and_add_billing_month_label(mocker: MockerFixture, file_name, is_called, init_eventbridge, app):
    spy = mocker.spy(app, 'add_label')
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    issue_detail = IssueDetail.from_issue_body(issue_body)
    app.check_and_add_billing_month_label(
        issue_detail,
        issue_body,
    )
    if is_called:
        spy.assert_called_once()
        # 引数がインスタンス時のassert_called_once_withがうまく動作しないため、下記実装にしている(インスタンスの比較になるので当たり前のことだが)
        assert spy.call_args[0][0].add_label == 'BillingMonth::2024-04'
    else:
        spy.assert_not_called()


@pytest.mark.parametrize('file_name,expected', [
    ('issue_wip_to_done.json', True),
    ('issue_closed_state_change_label.json', True),
    ('issue_backlog_to_wip.json', False),
])
def test_check_done(file_name, expected, app):
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    issue_detail = IssueDetail.from_issue_body(issue_body)
    assert app.check_done(issue_body, issue_detail,
                          LabelChangeState.CURRENT) == expected


@pytest.mark.parametrize('file_name,is_called', [
    ('issue_backlog_to_wip.json', True),
    ('issue_wip_to_done.json', False),
])
def test_check_and_add_health_on_track_label_and_schedule(
    mocker: MockerFixture, file_name, is_called,
    init_eventbridge, init_scheduler, app
):
    spy = mocker.spy(app, 'add_label')
    spy2 = mocker.spy(app, 'set_schedule')
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    issue_detail = IssueDetail.from_issue_body(issue_body)
    app.check_and_add_health_on_track_label_and_schedule(
        issue_body,
        issue_detail,
    )
    if is_called:
        spy.assert_called_once()
        assert spy.call_args[0][0].add_label.value == IssueLabel.HEALTH_ON_TRACK.value
        spy2.assert_called_once()
        assert spy2.call_args[0][0].add_label.value == IssueLabel.HEALTH_NEEDS_ATTENTION.value
        assert spy2.call_args[0][1].value == GitLabIssueEbSchedulerGroup.HEALTH_NEEDS_ATTENTION.value

    else:
        spy.assert_not_called()
        spy2.assert_not_called()


@pytest.mark.parametrize('file_name,expected', [
    ('issue_backlog_to_wip.json', True),
    ('issue_wip_to_done.json', False),
])
def test_check_wip(file_name, expected, app):
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    assert app.check_wip(issue_body, LabelChangeState.CURRENT) == expected


@pytest.mark.parametrize('file_name,expected', [
    ('issue_backlog_to_wip.json', True),
    ('issue_wip_to_done.json', False),
])
def test_check_not_wip(file_name, expected, app):
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    assert app.check_not_wip(issue_body, LabelChangeState.PREVIOUS) == expected


@pytest.mark.parametrize('file_name,label_change_state,expected', [
    ('issue_wip_to_done.json', LabelChangeState.PREVIOUS, True),
    ('issue_wip_to_done.json', LabelChangeState.CURRENT, False),
    ('issue_backlog_to_wip.json', LabelChangeState.PREVIOUS, True),
])
def test_check_not_done(file_name, label_change_state, expected, app):
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    assert app.check_not_done(issue_body, label_change_state) == expected


@pytest.mark.parametrize('input,mock_number_of_schedule,expeted', [
    (
        (
            12345, 1,
            GitLabIssueEbSchedulerGroup.HEALTH_NEEDS_ATTENTION,
        ),
        1,
        True,
    ),
    (
        (
            67890, 1,
            GitLabIssueEbSchedulerGroup.HEALTH_NEEDS_ATTENTION,
        ),
        0,
        False,
    ),
])
def test_check_schedule_exists(mocker: MockerFixture, input, mock_number_of_schedule, expeted, app):
    mock = mocker.patch.object(app.scheduler, 'list_schedules')
    mock.return_value = {'Schedules': [
        {'Name': '12345_1'}] * mock_number_of_schedule}
    assert app.check_schedule_exists(*input) == expeted


@pytest.mark.parametrize('freeze_data,expected', [
    ('2024-05-05', datetime(2024, 5, 8, tzinfo=JST)),
    ('2024-05-06', datetime(2024, 5, 8, tzinfo=JST)),
    ('2024-05-07', datetime(2024, 5, 9, tzinfo=JST)),
    ('2024-05-08', datetime(2024, 5, 10, tzinfo=JST)),
    ('2024-05-09', datetime(2024, 5, 13, tzinfo=JST)),
    ('2024-05-10', datetime(2024, 5, 14, tzinfo=JST)),
    ('2024-05-11', datetime(2024, 5, 15, tzinfo=JST)),
])
def test_calculate_schedule_date(freeze_data, expected, app):
    with freeze_time(freeze_data, -9):
        assert app.calculate_schedule_date() == expected


@pytest.mark.parametrize('input,schedule_exists,expected_schedule_name', [
    (
        (
            EventBridgeAddLabelDetail(
                12345, 1, IssueLabel.HEALTH_NEEDS_ATTENTION),
            GitLabIssueEbSchedulerGroup.HEALTH_NEEDS_ATTENTION,
        ),
        False,
        '12345_1'
    ),
    (
        (
            EventBridgeAddLabelDetail(
                67890, 2, IssueLabel.HEALTH_NEEDS_ATTENTION),
            GitLabIssueEbSchedulerGroup.HEALTH_NEEDS_ATTENTION,
        ),
        True,
        '67890_2'
    ),
    (
        (
            EventBridgeAddLabelDetail(
                67890, 2, IssueLabel.HEALTH_AT_RISK),
            GitLabIssueEbSchedulerGroup.HEALTH_AT_RISK,
        ),
        True,
        '67890_2'
    ),
])
def test_set_schedule(input, schedule_exists, expected_schedule_name, init_scheduler, app):
    schduler = boto3.client('scheduler')
    # list_schedulesのprefixパラメータがmotoで未実装のため修正したほうがよい
    if schedule_exists:
        schduler.create_schedule(
            FlexibleTimeWindow={
                'Mode': 'OFF',
            },
            Name=f'{input[0].project_id}_{input[0].iid}',
            GroupName=input[1].value,
            ScheduleExpression='at(2024-01-01T00:00:00)',
            Target={
                'Arn': 'test',
                'RoleArn': 'test',
            }
        )
        app.set_schedule(*input)
        actual_schedule = schduler.get_schedule(
            GroupName=input[1].value,
            Name=expected_schedule_name
        )
        assert actual_schedule['Name'] == expected_schedule_name
        # updateされていることを確認。spyしてcallしたほうが明確になる
        assert actual_schedule['Target']['Arn'] == os.environ['EVENT_BUS_ARN']
    else:
        app.set_schedule(*input)
        actual_schedule = schduler.get_schedule(
            GroupName=input[1].value,
            Name=expected_schedule_name
        )
        assert actual_schedule['Name'] == expected_schedule_name


@pytest.mark.parametrize('file_name,is_called,expected_label', [
    ('issue_assigned_current_done_not_task_level.json',
     True, IssueLabel.TASK_LEVEL_100),
    ('issue_assigned_200user_current_done_not_task_level.json',
     True, IssueLabel.TASK_LEVEL_200),
    ('issue_assigned_current_done_task_level.json', False, None),
    ('issue_wip_to_done.json', False, None),
])
def test_check_and_add_task_level_label(mocker: MockerFixture, file_name, is_called, expected_label, init_eventbridge, app):
    spy = mocker.spy(app, 'add_label')
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    issue_detail = IssueDetail.from_issue_body(issue_body)
    app.check_and_add_task_level_label(
        issue_body,
        issue_detail,
    )
    if is_called:
        spy.assert_called_once()
        assert spy.call_args[0][0].add_label.value == expected_label.value
    else:
        spy.assert_not_called()


@pytest.mark.parametrize('file_name,is_called', [
    ('issue_wip_to_done.json', True),
    ('issue_backlog_to_wip.json', False),
])
def test_check_leave_wip_and_reset_health_label(
    mocker: MockerFixture, file_name, is_called,
    init_eventbridge, init_scheduler, app
):
    spy = mocker.spy(app, 'delete_schedule_if_exist')
    spy2 = mocker.spy(app, 'add_label')
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    issue_detail = IssueDetail.from_issue_body(issue_body)
    app.check_leave_wip_and_reset_health_label(
        issue_body,
        issue_detail,
    )
    if is_called:
        assert spy.call_count == 2
        expected_schedule_groups = [GitLabIssueEbSchedulerGroup.HEALTH_NEEDS_ATTENTION,
                                    GitLabIssueEbSchedulerGroup.HEALTH_AT_RISK]
        for i, group in enumerate(expected_schedule_groups):
            assert spy.call_args_list[i][0][1].value == group.value
        spy2.assert_called_once()
        assert spy2.call_args[0][0].add_label.value == IssueLabel.HEALTH_ON_TRACK.value


@pytest.mark.parametrize('schedule_exists', [True, False])
def test_delete_schedule_if_exist(schedule_exists, init_scheduler, app):
    if schedule_exists:
        app.scheduler.create_schedule(
            FlexibleTimeWindow={
                'Mode': 'OFF',
            },
            Name='12345_1',
            GroupName=GitLabIssueEbSchedulerGroup.HEALTH_NEEDS_ATTENTION.value,
            ScheduleExpression='at(2024-01-01T00:00:00)',
            Target={
                'Arn': 'test',
                'RoleArn': 'test',
            }
        )
    app.delete_schedule_if_exist(
        IssueDetail(12345, 1, '', '', 123, IssueState.OPEN),
        GitLabIssueEbSchedulerGroup.HEALTH_NEEDS_ATTENTION
    )


@pytest.mark.parametrize('file_name,is_called', [
    ('issue_on_track_to_needs_attention_keeping_wip.json', True),
    ('issue_needs_attention_additional.json', False),
])
def test_check_and_add_health_needs_attention_label_and_schedule(
    mocker: MockerFixture, file_name,
    is_called, init_scheduler, app
):
    spy = mocker.spy(app, 'delete_schedule_if_exist')
    spy2 = mocker.spy(app, 'set_schedule')
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    issue_detail = IssueDetail.from_issue_body(issue_body)
    app.check_and_add_health_needs_attention_label_and_schedule(
        issue_body,
        issue_detail,
    )
    if is_called:
        spy.assert_called_once()
        assert spy.call_args[0][1].value == GitLabIssueEbSchedulerGroup.HEALTH_NEEDS_ATTENTION.value
        spy2.assert_called_once()
        assert spy2.call_args[0][1].value == GitLabIssueEbSchedulerGroup.HEALTH_AT_RISK.value


@pytest.mark.parametrize('file_name,expected', [
    ('issue_on_track_additional.json', True),
    ('issue_on_track_removal.json', False),
])
def test_check_on_track(file_name, expected, app):
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    assert app.check_on_track(issue_body, LabelChangeState.CURRENT) == expected


@pytest.mark.parametrize('file_name,expected', [
    ('issue_needs_attention_additional.json', True),
    ('issue_needs_attention_removal.json', False),
])
def test_check_needs_attention(file_name, expected, app):
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    assert app.check_needs_attention(
        issue_body, LabelChangeState.CURRENT) == expected


def test_get_issues_closed_by_mr(init_secret_manager, mock_requests_get_for_gitlab_closes_issues, app):
    res = app.get_issues_closed_by_mr(123, 1)
    assert type(res) is list
    mock_requests_get_for_gitlab_closes_issues.assert_called_once_with(
        'https://gitlab.com/api/v4/projects/123/merge_requests/1/closes_issues',
        headers={'PRIVATE-TOKEN': 'test'}
    )


def test_assign_user_and_add_wip_if_backlog_exists(mocker: MockerFixture, init_eventbridge, app):
    spy = mocker.spy(app, 'set_assignees')
    spy2 = mocker.spy(app, 'add_label')
    issue_list = mock_value_requests_get_for_gitlab_closes_issues
    app.assign_user_and_add_wip_if_backlog_exists(
        MergeRequestDetail(123, 1, '', app.EventAction.OPEN, 456, {}),
        issue_list,
    )
    spy.assert_called_once()
    assert spy.call_args[0][0].user_id == 456
    spy2.assert_called_once()
    assert spy2.call_args[0][0].add_label.value == IssueLabel.STATUS_WIP.value


def test_check_and_add_review_label(mocker: MockerFixture, init_eventbridge, app):
    spy = mocker.spy(app, 'add_label')
    issue_list = mock_value_requests_get_for_gitlab_closes_issues
    app.check_and_add_review_label(
        MergeRequestDetail(
            123, 1, '', app.EventAction.UPDATE, 456,
            {
                'draft': {'current': False}
            }
        ),
        issue_list,
    )
    spy.assert_called_once()
    assert spy.call_args[0][0].add_label.value == IssueLabel.STATUS_REVIEW.value


def test_get_gitlab_api_headers(init_secret_manager, app):
    res = app.get_gitlab_api_headers()
    assert res == {'PRIVATE-TOKEN': 'test'}
