import json
from pathlib import Path
import pytest
from gitlab_issue_manage.models import IssueDetail, MergeRequestDetail, EventAction


@pytest.mark.parametrize('file_name,expected', [
    ('issue_assigned.json', 'user123'),
    ('issue_label_change.json', None),
])
def test_from_issue_body(file_name, expected):
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        issue_body = json.load(f)
    issue_detail = IssueDetail.from_issue_body(issue_body)
    assert issue_detail.username == expected


@pytest.mark.parametrize('file_name,expected', [
    (
        'mr_open_action.json',
        MergeRequestDetail(
            12345678,
            2,
            "user123/project-name",
            EventAction.OPEN,
            123456,
            {
                "merge_status": {"previous": "preparing", "current": "checking"},
                "updated_at": {
                    "previous": "2024-05-29 07:15:00 UTC",
                    "current": "2024-05-29 07:15:01 UTC"
                },
                "prepared_at": {"previous": None, "current": "2024-05-29 07:15:01 UTC"}
            }
        ),
    ),
])
def test_from_mr_body(file_name, expected):
    file_path = Path('events') / file_name
    with open(file_path, 'r') as f:
        mr_body = json.load(f)
    mr_detail = MergeRequestDetail.from_mr_body(mr_body)
    assert mr_detail == expected
